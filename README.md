# SEO Workshop

This repository contains an app and exercises for the SEO Worksop conducted at 2023-01-13. Please read this readme file carefully.

The resources necessary to complete the workshop are located in the `docs` directory:
- [exercises](docs/exercises.md) (`docs/exercises.md`)
- [cheatsheet](docs/cheatsheet.md) (`docs/cheatsheet.md`) - it contains the same information as the [presentation](https://docs.google.com/presentation/d/1SD0MnPd3SbC1wanuArp5PdwU1ykwzG5EJOeUeqQaJYg/), please refer to it if you got lost or you want to remind something that was described before

❗**IMPORTANT**❗ Before starting any exercise, please create your branch so you won't accidentally push anything to the main branch!

## Requirements
This project requires Node.js 18.

## Getting started
1. Install dependencies: `pnpm install`
2. Start the project (frontend + backend): `pnpm start`

## Useful scripts

### `pnpm start`
Starts the application (both frontend and backend)

### `pnpm crawl`
Runs simple crawler in the application
    
ℹ️ Make sure that server is up and running (`pnpm start`) before invoking this command.

ℹ️ If the crawler fails to find any page, please run it with `--spa-safe-mode` option (`pnpm crawler --spa-safe-mode`). It will take more time but it should work fine.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.
