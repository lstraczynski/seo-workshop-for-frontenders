/* global require, process */

require('dotenv/config');

const puppeteer = require('puppeteer');
const colors = require('colors');
const { table } = require('table');

const APP_URL = `http://localhost:${process.env.VITE_SERVER_PORT}/`;
const WAIT_FOR_NETWORK_IDLE_SWITCH = process.argv.includes('--spa-safe-mode');

crawl();

async function crawl() {
  const results = {
    crawled: new Set(),
    indexed: new Set(),
    markedAsNoindex: new Set(),
    unfollowableLinks: new Set(),
    unfollowablePages: new Set(),
  };
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await discover(page, APP_URL, results);
  await browser.close();

  printSummary(results);
}

async function discover(page, url, results) {
  await page.goto(url);
  if (WAIT_FOR_NETWORK_IDLE_SWITCH) {
    await page.waitForNetworkIdle();
  }
  const [robots, links] = await Promise.all([
    getRobotsTag(page),
    getPageLinks(page),
  ]);

  console.log(`Page crawled: ${url}`);
  results.crawled.add(url);

  if (robots.index) {
    results.indexed.add(url);
  } else {
    results.markedAsNoindex.add(url);
  }
  if (!robots.follow) {
    results.unfollowablePages.add(url);
    return;
  }
  if (!links.length) {
    return;
  }

  return links.reduce(async (previousPageDiscovery, link) => {
    await previousPageDiscovery;
    if (results.crawled.has(link.href)) {
      return;
    }
    if (!link.follow) {
      results.unfollowableLinks.add(link.href);
      return;
    }
    return discover(page, link.href, results);
  }, Promise.resolve());
}

async function getRobotsTag(page) {
  return await page.evaluate(() => {
    const robots = document.querySelector('meta[name=robots]');
    if (!robots) {
      return { index: true, follow: true };
    }
    const directives = robots.content
      ?.split(',')
      .map((directive) => directive?.trim())
      .filter(Boolean);

    return {
      index: directives.includes('none')
        ? false
        : !directives.includes('noindex') || directives.includes('all'),
      follow: directives.includes('none')
        ? false
        : !directives.includes('nofollow') || directives.includes('all'),
    };
  });
}

async function getPageLinks(page, onlyFollowable = false) {
  const foundLinks = await page.$$eval('a[href]', (links) =>
    links.map((link) => {
      const rel = link.rel.split(/\s+/).filter(Boolean);
      return {
        href: link.href,
        follow: !rel.includes('nofollow'),
      };
    })
  );

  return onlyFollowable
    ? foundLinks.filter(({ follow }) => follow)
    : foundLinks;
}

function printSummary(results) {
  console.log(colors.blue(colors.bold('\nCrawling summary:')));
  console.log(
    table(
      [
        ['crawled', 'indexed', 'non-indexable', 'non-followable pages'],
        [
          results.crawled.size,
          results.indexed.size,
          results.markedAsNoindex.size,
          results.unfollowablePages.size,
        ],
      ],
      {
        columns: [
          { alignment: 'center' },
          { alignment: 'center' },
          { alignment: 'center' },
          { alignment: 'center' },
        ],
      }
    )
  );
}
