- [Intro](#intro)
  - [Library API](#library-api)
  - [Library usage](#library-usage)
- [Exercise 1 - Basic metadata](#exercise-1---basic-metadata)
- [Exercise 2 - Indexing](#exercise-2---indexing)
  - [2a. Crawlability of the site](#2a-crawlability-of-the-site)
  - [2b. Irrelevant pages](#2b-irrelevant-pages)
  - [2c. Sensitive information](#2c-sensitive-information)
- [Exercise 3 - Duplicate content](#exercise-3---duplicate-content)
- [Exercise 4 - URL keywords\*](#exercise-4---url-keywords)
- [Exercise 5 - New app language\*\*](#exercise-5---new-app-language)
- [Solution for Exercises 1-4](#solution-for-exercises-1-4)


# Intro

In this project we'll be using `@unhead/vue` package to manipulate metadata on our pages.

## Library API
The library exposes several useful composables but for this workshop we'll focus only on `useHead` in this workshop.

Here's an example of how to use the composable:
```typescript
useHead({
  title: 'Great Page Title',
  meta: [
    {
      name: 'description',
      content: 'This description is very descriptive but does not exceed 160 characters',
    },
    {
      name: 'robots',
      content: () => $route.fullPath.length % 2 === 0 ? 'index' : 'noindex',
    },
  ],
})
```

Please refer to [API docs](https://unhead.harlanzw.com/guide/guides/usehead) for more information.

ℹ️ **INFORMATION:** Although the library exposes some other composables (ex. `useSeoMeta`), `useHead` **is the recommended way** of setting meta tags **during this workshop** as the object's structure that this composable consumes, reflects the structure of meta tags in `head` of the document. Thanks to that you'll get better undestanding of how meta tags look like in the HTML code and you'll be able to put the knowledge to use with any other tool or framework.

## Library usage
Simply import the composable in any of your components and call it in the `setup()` of your component.

The library gives you a flexibility of setting different metadata in different components, and the resulting tags will be merged, deduplicated (if necessary) and rendered in the `head` section.

Some tips for using the library:
- You can set default meta tags in App's main component (`App.vue` in our case)
- If you don't set any metadata in your route components, those pages will inherit default metadata
- When using `useHead` composable in child component (doesn't have to route component, can be **any** component), it will merge provided metadata with ones set in parent components (see [tags deduping](https://unhead.harlanzw.com/guide/guides/handling-duplicates) section in API docs)
- Keep in mind th you don't need to provide all the metadata every time you use the composable, only the things you need to add/modify on for the specific page/component.

# Exercise 1 - Basic metadata

Now that you're familiar with the `useHead`, let's add the following meta tags to the pages:

| Page | Route name | Title | Description |
|---|---|---|---|
| Home page | `HOME` | Homepage \| AirDB | The most popular Airports and Airlines database for aviation enthusiasts |
| Airports list page | `AIRPORTS_LIST` | Airports \| AirDB | Browse through thousands of Airports from all over the world in the best aviation database! |
| Airport details page | `AIRPORT_DETAILS` | {airportName} airport details \| AirDB | Useful information about {airportName} ({airportIataCode}) in {countryName} |
| Airlines list page | `AIRLINES_LIST` | Airlines \| AirDB | Browse through thousands of Airlines from all over the world in the best aviation database! |
| Airline details page | `AIRLINE_DETAILS` | Information about {airlineName} \| AirDB | Detailed information about {airlineName} ({airlineIataCode}) airline from {airlineCountry} |

**Remember:** There's more than one way to achieve that. Pick the best way having extendability and maintainability in mind.

# Exercise 2 - Indexing

## 2a. Crawlability of the site
As a first step, let's verify if every page on our website is reachable for crawlers. If you spot something that might cause an issue and prevent from reaching the specific page, fix it.

💡 **HINT:** To verify how many pages could be discovered, you can run a simple crawler created for this workshop by executing `npm run crawl` command. Remember to start the server first! (`npm run start`)

💡 **HINT:** Read carefully what creators of the website are bragging about on their homepage and what does it mean in terms of crawlability.

## 2b. Irrelevant pages
You have been asked by our Product team to make sure that some irrelevant pages won't be indexed by Google.

The requirement is: airline details pages for inactive airlines should not be indexed by Google (example: http://localhost:5714/airline/5436)

Let's extend our solution implemented in previous exercise and make sure that bots won't discover those pages.

## 2c. Sensitive information
We have some links in our app that are pointing to some sensitive resources (Admin Panel page) that shouldn't be accessed and indexed by bots. Despite our efforts to convince the Product team to remove the "Admin Panel" link from the header, we failed, and we are required to find a solution that will prevent Admin Panel Login Page from being indexed and displayed in Google Search Result page.

# Exercise 3 - Duplicate content
When we have a single page that's accessible by multiple URLs, or different pages with similar content, Google sees these as duplicate versions of the same page. Google will choose one URL as the canonical version and crawl that, and all other URLs will be considered duplicate URLs and crawled less often. If we don't explicitly tell Google which URL is canonical, Google will make the choice for us.

Does this apply to our app? To find out let's answer first the following questions:
- Are those URLs different or the same for Google?
  - /airline/5436
  - /airline/5436/
  - /airline/5436?utm_campaign=fb
  - /airline/5436/?utm_campaign=fb
- Is there a possibility to see exactly same content when visiting different URLs?
- Do we have features that may cause serving the same content under different URLs (ex. by setting a query param)?

If we have have answered "yes" for any of abovementioned questions, we should consider telling Google which page is canonical and which page should appear in the search results page. Please implement such solution if necessary.

# Exercise 4 - URL keywords*
A site's URL structure should be as simple as possible. Your content should be organized in a way so that URLs are constructed logically and in a manner that is most intelligible to humans.

Let's take a look at our pages and verify if the URLs are understandable:

| Page | Route name | Path |
|---|---|---|
| Home page | `HOME` | `/` |
| Airports list page | `AIRPORTS_LIST` | `/airports` |
| Airport details page | `AIRPORT_DETAILS` | `/airports/:id` (ex. `/airports/123`) |
| Airlines list page | `AIRLINES_LIST` | `/airlines` |
| Airline details page | `AIRLINE_DETAILS` | `/airlines/:id` (ex. `/airlines/123`) |

If you're still not sure, answer yourself these quesiton:
- Can you figure out what content you'll get only by looking at the page address (without visiting the page)? 
- Are those URLs descriptive enough?
- Are IDs used in URLs self-explanatory?

💡 **HINT:** Our backend is prepared to fetch data by both: IDs and slugs.

# Exercise 5 - New app language**
We want to add another language in the application apart from English. In our project we have `vue-i18n` library included which we're going to use. 

The goal of this challenge is to:
- Use the `vue-i18n` library to add support for many languages in the app
- Add an ability (buttons or dropdown in the navbar) to switch the language in the app
- Add `language` parameter to the beginning of the path to clearly indicate which language is this page in
- Make the keywords in the URL translated based on the selected language
- Make sure that all meta tags contain valid properties (links, urls, content)

---

`*` This is an extra exercise that you can do on your own as it doesn't require any extra SEO knowledge. If you finished the previous exercises you can jump on it right away and implement this feature.

`**` The exercise requires more time to implement so we won't be doing it during this workshop. You can do it on your own. Check out the [cheatsheet](cheatsheet.md) and the presentation to find information that will help you to complete this challenge.


# Solution for Exercises 1-4
Solutions for exercises can be found on the following branches:
- [simple-solution](https://gitlab.com/lstraczynski/seo-workshop-for-frontenders/-/tree/simple-solution) - this branch contains simple solutions for problems in the exercises
