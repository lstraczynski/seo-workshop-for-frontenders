# SEO Workshop cheetsheet

Basically everything that was in the [presentation](https://docs.google.com/presentation/d/1SD0MnPd3SbC1wanuArp5PdwU1ykwzG5EJOeUeqQaJYg/) but in Markdown format! 🎉

## How Google Search works?
Google Search works in three stages:

1. Crawling
2. Indexing
3. Serving search results

Keep in mind that not all pages make through each stage.

### 1. Crawling
The first stage is finding out what pages exist on the web. Google constantly looks for new and updated pages and add them to its list of known pages. This is known as "URL discovery". The pages are discovered when, for example, Google follows a link from a known page to a new page. Other pages are discovered when you submit a list of pages (a sitemap) for Google to crawl.

Once Google discovers a page's URL, it may visit (crawl) the page to find out what's on it. However, Googlebot doesn't crawl all the pages it discovered. Some pages may be disallowed for crawling by the site owner, other pages may not be accessible without logging in to the site.

During the crawl, Google renders the page and runs any JavaScript it finds using a recent version of Chrome, similar to how your browser renders pages you visit. Rendering is important because websites often rely on JavaScript to bring content to the page, and without rendering Google might not see that content.

### 2. Indexing
After a page is crawled, Google tries to understand what the page is about. This stage is called indexing and it includes processing and analyzing the textual content and key content tags and attributes, such as `<title>` elements, meta tags, alt attributes, images, videos, and more.

During the indexing process, Google determines if a page is a duplicate of another page on the internet or canonical. The canonical is the page that may be shown in search results. 

The collected information about the canonical page and its cluster may be stored in the Google index, which is basically a large database.

### 3. Serving search results
When a user enters a query, our machines search the index for matching pages and return the results we believe are the highest quality and most relevant to the user's query. Relevancy is determined by hundreds of factors, which could include information such as the user's location, language, and device type.

**Useful links**
- [In-depth guide to how Google Search works](https://developers.google.com/search/docs/fundamentals/how-search-works)

## Basic meta tags
Meta tags are HTML tags used to provide additional information about a page to search engines and other clients. Clients process the meta tags and ignore those they don't support. Meta tags are added to the `<head>` section of your HTML page.

### Page title
Title links are critical to giving users a quick insight into the content of a result and why it's relevant to their query. It's often the primary piece of information people use to decide which result to click on, so it's important to use high-quality title text on your web pages.

The title tag of a web page is meant to be an accurate and concise description of a page's content.

**Example HTML**
```html
<head>
  <title>ExampleCompany: you search, we deliver search results!</title>
</head>
```

**Useful links**
- [Best practices for influencing title links](https://developers.google.com/search/docs/appearance/title-link#page-titles)

### Page meta description
A meta description is an HTML element that provides a brief summary of a web page. A page’s meta description tag is displayed as part of the search snippet in a search engine results page and is meant to give the user an idea of the content that exists within the page and how it relates to their search query. Meta descriptions can technically be any length, but Google generally truncates snippets to 155-160 characters. It's best to keep meta descriptions long enough that they're sufficiently descriptive. General recommendation is for descriptions to be between 50 and 160 characters.

**Example HTML**
```html
<head>
  <meta name="description" content="ExampleCompany is the aircraft and airport search engine that will get you your results in a matter of seconds. You search, we'll deliver the results!">
</head>
```

**Useful links**
- [Best practices for creating quality meta descriptions](https://developers.google.com/search/docs/appearance/snippet#meta-descriptions)

## Crawling and indexing

### Crawling
Google can follow your links only if they use proper `<a>` tags with resolvable URLs. 

**Examples**
- ✅ `<a href="https://example.com">`
- ✅ `<a href="/relative/path/file">`
- ❌ `<a routerLink="some/path">`
- ❌ `<span href="https://example.com">`
- ❌ `<a onclick="goto('https://example.com')">`
- ❌ `<button onclick="router.push('/some/path')">`

✅ - will follow | ❌ - won't follow

**Useful links**
- [Make your links crawlable](https://developers.google.com/search/docs/crawling-indexing/links-crawlable)

### Indexing
To control the indexing of your pages you can use `<meta name="robots">` tag. The robots meta tag lets you utilize a granular, page-specific approach to controlling how an individual page should be indexed and served to users in Google Search results.

**Example HTML**
```html
<head>
  <meta name="robots" content="index,follow">
</head>
```

The following table shows part of the directives that Google accepts as a `content` property in the `robots` meta tag:

| Directive | Description |
|---|---|
| `all` | There are no restrictions for indexing - Google can index this page and follow the links. This directive is the default value.<br>Equivalent to: `index, follow` |
| `noindex` | Do not show this page in search results. If you don't specify this directive, the page may be indexed and shown in search results |
| `nofollow` | Do not follow the links on this page. If you don't specify this directive, Google may follow the links on the page to discover other pages |
| `none` | Equivalent to `noindex, nofollow` |

**Useful links**
- [Robots meta tag](https://developers.google.com/search/docs/crawling-indexing/robots-meta-tag)
- [Valid indexing and serving directives](https://developers.google.com/search/docs/crawling-indexing/robots-meta-tag#directives)

## Canonical URL
If you have a single page that's accessible by multiple URLs, or different pages with similar content, Google sees these as duplicate versions of the same page. Google will choose one URL as the canonical version and crawl that, and all other URLs will be considered duplicate URLs and crawled less often. If you don't explicitly tell Google which URL is canonical, Google will make the choice for you, or might consider them both of equal weight, which might lead to unwanted behavior.

The pages don't need to be absolutely identical to be marked as duplicate; minor changes in sorting or filtering of list pages **don't make** the page unique (for example, sorting by price or filtering by item color).

To prevent such behavior, you can specify which URL is a canonical URL of the page that Google thinks is most representative from a set of duplicate pages on your site. 

**Example**

If you have sorting or filtering on your page those URLs would probably be marked as duplicates:
- `/airports`
- `/airports?name=foo&sortBy=name`
- `/airports?sortBy=name`

Without proper canonicalization, Google will make the choice for you and it might choose the wrong URL (like for example the one with `foo` name filter).

💡 **TIP:** Use absolute URLs rather than paths with the `canonical` link tag

**Example HTML**
```html
<head>
  <link rel="canonical" href="https://example.com/pl/">
</head>
```

**Useful links**
- [Canonical link tag](https://developers.google.com/search/docs/crawling-indexing/consolidate-duplicate-urls#rel-canonical-link-method)

## Human-readable and SEO friendly URLs
A site's URL structure should be as simple as possible. Consider organizing your content so that URLs are constructed logically and in a manner that is most intelligible to humans. When possible, use readable words rather than long ID numbers in your URLs.

### Do's and dont's

**Use keywords in favor of identifiers**
- ✅ `/airports/lotnisko-chopina-okecie-waw`
- ❌ `/airports/472827`

**Keep the URL nesting to a minimum**
- ✅ `/airports/lotnisko-chopina-okecie-waw`
- ❌ `/airdb/catalog/category/airport/entity/lotnisko-chopina-okecie-waw`

**Prefer human-readable keywords instead of shortcuts**
- ✅ `/airports/chopin-airport-waw/departures`
- ❌ `/ap/chopin-airport-waw/dep`

**Prefer hyphens (-) for separating keywords**
- ✅ `/airports/lotnisko-chopina-okecie-waw`
- ❌ `/airports/lotnisko_chopina_okecie_waw`
- ❌ `/airports/lotnisko,chopina,okecie,waw`

**Prefer keywords without language-specific characters (or use UTF-8 encoding if required)**
- ✅ `/airports/lotnisko-chopina-okecie-waw`
- ✅ `/search/ok%C4%99cie`
- ❌ `/airports/lotnisko-chopina-okęcie-waw`
- ❌ `/杂货/薄荷`
- ❌ `/🦙✨`

**Specify the language in the URL by either using TLD or path segment**
- ✅ `www.example.com/pl/blog`
- ✅ `www.example.pl/blog`
- ❌ `www.example.com/blog` <- same URL for all language versions

**Use localized keywords for different languages**
- ✅ `/pl/lotniska/lotnisko-chopina-okecie-waw`
- ❌ `/pl/airports/lotnisko-chopina-okecie-waw`

**Useful links**
- [Keep a simple URL structure](https://developers.google.com/search/docs/crawling-indexing/url-structure)

## Localized versions of your page
If you have multiple versions of a page for different languages or regions, tell Google about these different variations. Doing so will help Google Search point users to the most appropriate version of your page by language or region.

In order to tell Google about other languages and region variants of a page you can use `<link rel="alternate" hreflang="lang_code"... >` elements in your page header. There are also other alternatives to that as using the sitemap or specifying HTTP response headers for your site.

**Example HTML**
```html
<head>
  <link rel="alternate" hreflang="pl" href="https://example.com/pl/warszawa/">
  <link rel="alternate" hreflang="en" href="https://example.com/en/warsaw/">
  <link rel="alternate" hreflang="uk" href="https://example.com/uk/varshava/">
</head>
```

**Useful links**
- [Tell Google about localized versions of your page](https://developers.google.com/search/docs/specialty/international/localized-versions)