import { useFetcher } from './use-fetcher';
import type { Airline } from '@/models/airline';
import { ref } from 'vue';

const fetcher = useFetcher();

export function useAirlinesData() {
  const airlines = ref<Airline[]>([]);

  const getAirlinesList = async (forceFetch: boolean = false) => {
    if (forceFetch) {
      airlines.value = [];
    }
    if (airlines.value.length) {
      return airlines.value;
    }
    airlines.value = await fetcher.get<Airline[]>('/airlines');
    return airlines.value;
  };

  const getAirlinesCount = async () => {
    const { count } = await fetcher.get<{ count: number }>('/airlines/count');
    return count;
  };

  const getAirlineDetails = async (id: number) => {
    return await fetcher.get<Airline>(`/airlines/${id}`);
  };

  return {
    airlines,
    getAirlinesList,
    getAirlinesCount,
    getAirlineDetails,
  };
}
