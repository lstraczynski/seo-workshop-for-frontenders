import { useFetcher } from './use-fetcher';

export interface SalaryManagementResponse {
  employeeFullName: string;
  salaryChangePercentage: number;
}

const fetcher = useFetcher();

export function useEmployeesManagement() {
  const manageRandomEmployee = async () => {
    return await fetcher.get<SalaryManagementResponse>(
      '/random-employee-salary-management'
    );
  };

  return {
    manageRandomEmployee,
  };
}
