import { computed, ref, type Ref } from 'vue';
import sortBy from 'lodash/sortBy';

export interface SortingOptions<T extends Record<string, any> = any> {
  items: Ref<T[]>;
  initialSorting?: keyof T;
}

export function useSorting<T extends Record<string, any> = any>(
  options: SortingOptions<T>
) {
  const sortingBy = ref(options.initialSorting);
  const sortedItems = computed(() =>
    sortingBy.value
      ? sortBy(options.items.value, sortingBy.value)
      : options.items.value
  );

  return {
    sortingBy,
    sortedItems,
  };
}
