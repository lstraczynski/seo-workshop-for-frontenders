import axios from 'axios';

const $http = axios.create({
  baseURL: '/api',
});

export function useFetcher() {
  return {
    async get<T = any>(url: string): Promise<T> {
      try {
        const { data } = await $http.get<T>(url);
        return data;
      } catch (err) {
        throw axios.isAxiosError(err) ? err.response : err;
      }
    },
  };
}
