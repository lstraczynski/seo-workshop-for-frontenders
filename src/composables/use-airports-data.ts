import { useFetcher } from './use-fetcher';
import type { Airport } from '@/models/airport';
import { ref } from 'vue';

const fetcher = useFetcher();

export function useAirportsData() {
  const airports = ref<Airport[]>([]);

  const getAirportsList = async (forceFetch: boolean = false) => {
    if (forceFetch) {
      airports.value = [];
    }
    if (airports.value.length) {
      return airports.value;
    }
    airports.value = await fetcher.get<Airport[]>('/airports');
    return airports.value;
  };

  const getAirportsCount = async () => {
    const { count } = await fetcher.get<{ count: number }>('/airports/count');
    return count;
  };

  const getAirportDetails = async (id: number) => {
    return await fetcher.get<Airport>(`/airports/${id}`);
  };

  return {
    airports,
    getAirportsList,
    getAirportsCount,
    getAirportDetails,
  };
}
