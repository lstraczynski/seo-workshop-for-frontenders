export interface Airport {
  id: number;
  name: string;
  slug: string;
  city: string;
  country: string;
  iata: string;
  coordinates: {
    lat: number;
    lng: number;
  };
}
