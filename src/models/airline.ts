export interface Airline {
  id: number;
  name: string;
  iata: string | null;
  icao: string | null;
  callsign: string;
  country: string;
  active: boolean;
}
