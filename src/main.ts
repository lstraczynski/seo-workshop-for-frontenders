import { createApp } from 'vue';
import { createHead } from '@unhead/vue';
import { createI18n } from 'vue-i18n';
import PrimeVue from 'primevue/config';
import App from './App.vue';
import router from './router';
import en from './i18n/en';

import './assets/main.css';
import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

const app = createApp(App);
const head = createHead();
// Ignore this until you reach: Exercise 5
const i18n = createI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en },
});

app.use(PrimeVue);
app.use(router);
app.use(head);
app.use(i18n);

app.mount('#app');
