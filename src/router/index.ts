import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import AirlinesListView from '../views/AirlinesListView.vue';
import AirlineDetailsView from '../views/AirlineDetailsView.vue';
import AirportsListView from '../views/AirportsListView.vue';
import AirportDetailsView from '../views/AirportDetailsView.vue';
import AdminPanelLoginView from '../views/AdminPanelLoginView.vue';
import AnotherInternalSystemView from '../views/AdminEmployeesPaymentManagementToolView.vue';

export enum RouteNames {
  HOME = 'HOME',
  AIRLINES_LIST = 'AIRLINES_LIST',
  AIRLINE_DETAILS = 'AIRLINE_DETAILS',
  AIRPORTS_LIST = 'AIRPORTS_LIST',
  AIRPORT_DETAILS = 'AIRPORT_DETAILS',
  ADMIN_PANEL = 'ADMIN_PANEL',
  ADMIN_EMPLOYEES_PAYMENT_MANAGEMENT_TOOL = 'EMPLOYEES_PAYMENT_MANAGEMENT_TOOL',
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: RouteNames.HOME,
      component: HomeView,
    },
    {
      path: '/airlines',
      name: RouteNames.AIRLINES_LIST,
      component: AirlinesListView,
    },
    {
      path: '/airlines/:id',
      name: RouteNames.AIRLINE_DETAILS,
      component: AirlineDetailsView,
      meta: {
        parentRoute: RouteNames.AIRLINES_LIST,
      },
    },
    {
      path: '/airports',
      name: RouteNames.AIRPORTS_LIST,
      component: AirportsListView,
    },
    {
      path: '/airports/:id',
      name: RouteNames.AIRPORT_DETAILS,
      component: AirportDetailsView,
      meta: {
        parentRoute: RouteNames.AIRPORTS_LIST,
      },
    },
    {
      path: '/admin/login',
      name: RouteNames.ADMIN_PANEL,
      component: AdminPanelLoginView,
    },
    {
      path: '/admin/employees-payment-management',
      name: RouteNames.ADMIN_EMPLOYEES_PAYMENT_MANAGEMENT_TOOL,
      component: AnotherInternalSystemView,
    },
  ],
});

export default router;
