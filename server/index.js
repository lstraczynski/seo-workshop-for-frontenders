/* global require, process */
require('dotenv/config');
const express = require('express');
const morgan = require('morgan');
const {
  randFirstName,
  randLastName,
  randNumber,
  randJobTitle,
} = require('@ngneat/falso');
const airports = require('./data/airports.json');
const airlines = require('./data/airlines.json');

const app = express();

app.use(express.json());
app.use(morgan('combined'));

app.get('/api/airports', (_, res) => {
  res.json(airports);
});

app.get('/api/airports/count', (_, res) => {
  res.json({ count: airports.length });
});

app.get('/api/airports/:idOrSlug', (req, res) => {
  const airportId = Number(req.params.idOrSlug);
  const airportSlug = req.params.idOrSlug;
  const airport = airports.find(
    ({ id, slug }) => id === airportId || slug === airportSlug
  );

  if (airport) {
    res.json(airport);
  } else {
    res.status(404).json({ error: 'NOT FOUND' });
  }
});

app.get('/api/airlines', (_, res) => {
  res.json(airlines);
});

app.get('/api/airlines/count', (_, res) => {
  res.json({ count: airlines.length });
});

app.get('/api/airlines/:idOrSlug', (req, res) => {
  const airlineId = Number(req.params.idOrSlug);
  const airlineSlug = req.params.idOrSlug;
  const airline = airlines.find(
    ({ id, slug }) => id === airlineId || slug === airlineSlug
  );

  if (airline) {
    res.json(airline);
  } else {
    res.status(404).json({ error: 'NOT FOUND' });
  }
});

app.get('/api/random-employee-salary-management', (_, res) => {
  res.json({
    employeeFullName: `${randFirstName()} ${randLastName()} (${randJobTitle()})`,
    salaryChangePercentage: randNumber({ min: -100, max: 100, precision: 1 }),
  });
});

console.log(`Backend listening on port: ${process.env.BACKEND_PORT}`);
app.listen(Number(process.env.BACKEND_PORT));
